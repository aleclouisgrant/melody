## Melody: the Game
A mobile rhythm game with clean graphics and simple but engaging gameplay.   


## Contributors:
Alec Grant   
Matthew Lew   
Robert Kolchmeyer   

## Git Stuff

To see the files that you changed since the last commit:   
Go to the root of the project directory   
This should be 'melody', and an "ls" should list Melody Project, README.md, maybe other things   
Run the following:   
$ git status

To commit your changes (locally):   
Go to the root of the project directory.   
Now run the following:   
$ git add *   
$ git commit -a   

Write the commit message, save and quit from whatever text editor pops up.   

This does not *push* the changes to bitbucket, this just commits them on your local machine.   

To push your changes:   
First commit them, then run:   
$ git push origin master   

To pull other people's changes:   
First commit any local changes you made. Then run:   
$ git pull origin master   

If it doesn't work, then there are likely merge conflicts. We should deal with these on a case-by-case basis, but to prevent these, don't work on the same file as someone else. So if one person is working on the MainMenu scene, let that person finish what they are doing and let them push their changes, then you can pull their changes and do what you want. We can all work on different files at the same time, but working on the same file at the same time becomes gnarly.   

If you feel like your local repository is hopelessly out of sync with what's on bitbucket, feel free to just delete the whole thing and re-clone it.