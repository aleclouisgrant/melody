﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndMenu : MenuScript {

    public Text text;

    new public void enableMenu()
    {
        text.text = (PlayerPrefs.GetFloat("VideoLatency") * 1000.0f).ToString() + " ms";
        base.enableMenu();
    }
}
