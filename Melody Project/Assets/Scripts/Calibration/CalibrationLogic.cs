﻿using UnityEngine;
using System.Collections;

public class CalibrationLogic : MonoBehaviour {

    public Collider2D ringCollider;
    public Animator ringAnimator;
    public Canvas startMenu;
    public Canvas endMenu;
    public Canvas areYouSureMenu;
    public GameObject note;
    public float velocityScale;

    float tolerance;

    float spawnDistance;
    float ringMiddle;

    float videoLatency;
    int numSamples;

    public static bool isPaused;

    float hitTime;
    float savedTimeToHit;

    int MAX_SAMPLES = 15;
    void Start()
    {

        videoLatency = 0;
        numSamples = 0;
        tolerance = 0.2f;

        velocityScale = 0.07f;
        RaycastHit2D hitinfo;
        hitinfo = Physics2D.Raycast(new Vector2(15f, 0f), new Vector2(-1f, 0f), Mathf.Infinity, -1);
        spawnDistance = hitinfo.distance;
        hitinfo = Physics2D.Raycast(new Vector2(0f, 0f), new Vector2(1f, 0f), Mathf.Infinity, -1);
        ringMiddle = hitinfo.distance + (15f - spawnDistance - hitinfo.distance) / 2;
        note.GetComponent<NoteScript>().velocityScale = velocityScale;
        note.GetComponent<NoteScript>().ringAnimator = ringAnimator;
        note.GetComponent<NoteScript>().ringCollider = ringCollider;
        note.GetComponent<CircleCollider2D>().offset = new Vector2(0f, 0f);

        startMenu = startMenu.GetComponent<Canvas>();

        isPaused = true;
        startMenu.GetComponent<MenuScript>().enableMenu();

    }

    //We use update when dealing with touch input functions. We use FixedUpdate for everything else.
    void Update()
    {
        if (isPaused)
        {
            return;
        }

        note.GetComponent<NoteScript>().startAngle = 0;

        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
        {
            RaycastHit2D left = Physics2D.Raycast(new Vector2(ringMiddle, 0f), new Vector2(-1f, 0f));
            RaycastHit2D right = Physics2D.Raycast(new Vector2(ringMiddle, 0f), new Vector2(1f, 0f));
            
            if (left.point.x != 0 && left.distance <= right.distance)
            {
                GameObject targetNote = left.collider.gameObject;
                float offset = Mathf.Abs(ringMiddle - targetNote.transform.position.x);
                if (offset >= tolerance)
                    offset = tolerance;
                if (ringMiddle - targetNote.transform.position.x < 0)
                    videoLatency += (ringMiddle - targetNote.transform.position.x + offset) / (velocityScale / Time.fixedDeltaTime);
                else if (ringMiddle - targetNote.transform.position.x > 0)
                    videoLatency += (ringMiddle - targetNote.transform.position.x - offset) / (velocityScale / Time.fixedDeltaTime);
                numSamples += 1;
            }
            else if (right.point.x != 0)
            {
                GameObject targetNote = right.collider.gameObject;
                float offset = Mathf.Abs(ringMiddle - targetNote.transform.position.x);
                if (offset >= tolerance)
                    offset = tolerance;
                if (ringMiddle - targetNote.transform.position.x < 0)
                {
                    videoLatency += (ringMiddle - targetNote.transform.position.x + offset) / (velocityScale / Time.fixedDeltaTime);
                }
                else if (ringMiddle - targetNote.transform.position.x > 0)
                    videoLatency += (ringMiddle - targetNote.transform.position.x - offset) / (velocityScale / Time.fixedDeltaTime);
                numSamples += 1;
            }
        }
    }

    void FixedUpdate()
    {
        if (isPaused)
        {
            return;
        }

        float spawnTime = hitTime - spawnDistance / (velocityScale / Time.fixedDeltaTime);
        if (Time.realtimeSinceStartup >= spawnTime && numSamples <= MAX_SAMPLES)
        {
            Instantiate(note);
            hitTime += 1.5f;
        }

        if (numSamples > MAX_SAMPLES)
        {
            isPaused = true;
            PlayerPrefs.SetFloat("VideoLatency", videoLatency / numSamples);
            PlayerPrefs.Save();
            endMenu.GetComponent<EndMenu>().enableMenu();
        }
    }

    public void startOver()
    {
        Application.LoadLevel(4);
    }

    public void exit()
    {
        Application.LoadLevel(0);
    }

    public void unpause()
    {
        hitTime = Time.realtimeSinceStartup + 5.0f;
        startMenu.GetComponent<MenuScript>().disableMenu();
        isPaused = false;
    }

    public void areYouSure()
    {
        savedTimeToHit = hitTime - Time.realtimeSinceStartup;
        isPaused = true;
        areYouSureMenu.GetComponent<MenuScript>().enableMenu();
    }

    public void areYouSureResume()
    {
        areYouSureMenu.GetComponent<MenuScript>().disableMenu();
        hitTime = Time.realtimeSinceStartup + savedTimeToHit;
        isPaused = false;
    }
}
