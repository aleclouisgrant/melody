﻿using UnityEngine;
using System.Collections;

public class RingTouchScript : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start () {
        animator = gameObject.GetComponent<Animator>();
	}

    void OnMouseDown()
    {
        animator.SetTrigger("Touched");
    }
}
