﻿using UnityEngine;
using System.Collections;

public class NoteScript : MonoBehaviour {

    public Collider2D ringCollider;
    public Animator ringAnimator;
    public Animator sphereAnimator;
    public float velocityScale;
    public float startAngle;
    public ParticleSystem explode;

    Color defaultParticleColor;

    Collider2D noteCollider;
    Animator noteAnimator;

    float spawnRadius;
    float killZone;

    Vector3 velocity;
    Vector3 startPoint;
    Vector3 center;

	// Use this for initialization
	void Start () {

        ringCollider = ringCollider.GetComponent<Collider2D>();
        ringAnimator = ringAnimator.GetComponent<Animator>();

        noteCollider = gameObject.GetComponent<Collider2D>();
        noteAnimator = gameObject.GetComponent<Animator>();

        sphereAnimator = sphereAnimator.GetComponent<Animator>();

        defaultParticleColor = explode.startColor;

        killZone = 0.1f;
        spawnRadius = 15f;

        startPoint = new Vector3(spawnRadius * Mathf.Cos(startAngle), spawnRadius * Mathf.Sin(startAngle), 10f);
        center = new Vector3(0f, 0f, 10f);
        velocity = center - startPoint;
        velocity.Normalize();
        velocity *= velocityScale;

        transform.position = startPoint;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Application.loadedLevelName.Equals("Calibration") && CalibrationLogic.isPaused)
        {
            return;
        }

        if (Application.loadedLevelName.Equals("MelodyGame") && MelodyGameLogic.isPaused)
        {
            return;
        }

        transform.position += velocity;
        if ((transform.position - center).magnitude < killZone)
        {
            velocity = new Vector3(0f, 0f, 0f);
            noteAnimator.SetTrigger("Hit");
            sphereAnimator.SetTrigger("Touched");
            if (Application.loadedLevelName.Equals("MelodyGame") && MelodyGameLogic.currentCombo != 0)
            {
                MelodyGameLogic.comboSet.Add(MelodyGameLogic.currentCombo);
                MelodyGameLogic.currentCombo = 0;
            }
            explode.startColor = new Color(255.0f, 0f, 0f);
            ParticleSystem newParticleSystem = (ParticleSystem)Instantiate(explode, transform.position, Quaternion.identity);
            Destroy(newParticleSystem.gameObject, newParticleSystem.startLifetime);
            Destroy(gameObject);
        }
	}

    void OnMouseDown()
    {
        if (Application.loadedLevelName.Equals("Calibration"))
        {
            return;
        }

        if (Application.loadedLevelName.Equals("MelodyGame") && MelodyGameLogic.isPaused)
        {
            return;
        }

        else if (noteCollider.IsTouching(ringCollider))
        {
            velocity = new Vector3(0f, 0f, 0f);
            ringAnimator.SetTrigger("Touched");
            noteAnimator.SetTrigger("Hit");
            MelodyGameLogic.hitNotes++;
            MelodyGameLogic.currentCombo++;
            explode.startColor = new Color(255, 255, 255);
            ParticleSystem newParticleSystem = (ParticleSystem)Instantiate(explode, transform.position, Quaternion.identity);
            Destroy(newParticleSystem.gameObject, newParticleSystem.startLifetime);
            Destroy(gameObject);
        }
    }


}
