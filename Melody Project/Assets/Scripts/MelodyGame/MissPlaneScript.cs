﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MissPlaneScript : MonoBehaviour {

    public MelodyGameLogic script;

    void Start()
    {
        script = script.GetComponent<MelodyGameLogic>();
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        script.onMiss();
    }
}
