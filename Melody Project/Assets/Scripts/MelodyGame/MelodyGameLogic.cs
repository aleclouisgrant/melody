﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MelodyGameLogic : MonoBehaviour {

    public Collider2D ringCollider;
    public Animator ringAnimator;
    public Animator sphereAnimator;
    public Canvas pauseMenu;
    public Canvas scoreMenu;
    public GameObject note;
    public float velocityScale;

    string songName;
    string songArtist;
    string songTitle;
    string difficultyLevel;
    float songTime;
    float previousFrameTime;
    float lastReportedPlayheadPosition;
    AudioSource audio;

    public static int hitNotes;
    public static int missNotes;
    public static int currentCombo;
    public static ArrayList comboSet;
    int totalNotes;
    Text comboText;
    Text countdownText;

    float SPAWN_RADIUS;     //radius of spawning circle
    float spawnDistance;    //distance of spawning circle from ring
    float ringMiddle;       //distance from origin to middle of ring

    float videoLatency;
    Vector2 noteColliderOffset;

    public static bool isPaused;

    TextAsset file;
    static int NUM_TIMERS = 0;
    int currentTimer = 0;

    struct timerEntry
    {
        public float hitTime;
        public float angle;
        public bool isActive;
    };

    timerEntry[] timer_table;

	// Use this for initialization
	void Start () {

        SPAWN_RADIUS = 15.0f;

        hitNotes = 0;
        missNotes = 0;
        currentCombo = 0;
        comboSet = new ArrayList();
        totalNotes = 0;
        foreach(Text textObject in gameObject.GetComponentsInChildren<Text>()) {
            if (textObject.text.Equals("C O M B O :"))
            {
                comboText = textObject;
            }
            if (textObject.text.Equals(""))
            {
                countdownText = textObject;
            }
        }

        // SongName is name of audio file without the .ogg
        songName = PlayerPrefs.GetString("SongName");
	    songArtist = PlayerPrefs.GetString("SongArtist");
        songTitle = PlayerPrefs.GetString("SongTitle");
	    difficultyLevel = PlayerPrefs.GetString("DifficultyLevel");

        //Initialize pause menu
        Text[] textComponents = pauseMenu.GetComponentsInChildren<Text>();
        foreach (Text i in textComponents)
        {
            switch (i.text)
            {
                case "Song Title - Song Artist":
                    i.text = songTitle + " - " + songArtist;
                    break;
                case "0":
                    i.text = difficultyLevel;
                    break;
            }
        }
        pauseMenu = pauseMenu.GetComponent<Canvas>();

        //Initialize score menu
        scoreMenu = scoreMenu.GetComponent<Canvas>();

        //Set character text
        if (PlayerPrefs.GetString("Character").Equals("Victor")) {
            textComponents = gameObject.GetComponentsInChildren<Text>();
            foreach (Text i in textComponents)
            {
                if (i.text.Equals("M E L O D Y"))
                {
                    i.text = "V I C T O R";
                    break;
                }
            }
        }

        //Initialize and start audio
        audio = gameObject.GetComponent<AudioSource>();
        audio.clip = (AudioClip)Resources.Load(songName, typeof(AudioClip));
        previousFrameTime = Time.timeSinceLevelLoad;
        songTime = Time.timeSinceLevelLoad;
        lastReportedPlayheadPosition = 0;
        audio.Play();

        //Get video latency
        videoLatency = PlayerPrefs.GetFloat("VideoLatency");
        if (videoLatency == 0 || float.IsInfinity(videoLatency) || float.IsNaN(videoLatency))
        {
            videoLatency = 0.05f;
        }
        videoLatency = 0;   //Disable video latency correction for now

        //Compute spawn distance, initialize note, compute collider offset
        RaycastHit2D hitinfo;
        hitinfo = Physics2D.Raycast(new Vector2(SPAWN_RADIUS, 0f), new Vector2(-1f, 0f), Mathf.Infinity, LayerMask.GetMask("Ignore Raycast"));
        spawnDistance = hitinfo.distance;
        note.GetComponent<NoteScript>().velocityScale = velocityScale;
        note.GetComponent<NoteScript>().ringAnimator = ringAnimator;
        note.GetComponent<NoteScript>().ringCollider = ringCollider;
        note.GetComponent<NoteScript>().sphereAnimator = sphereAnimator;
        note.GetComponent<NoteScript>().startAngle = 0;
        note.GetComponent<CircleCollider2D>().offset = new Vector2(0f, 0f);
        noteColliderOffset = note.GetComponent<CircleCollider2D>().offset;

        //Fill timer table from song file
        file = (TextAsset)Resources.Load(songName + "_" + difficultyLevel, typeof(TextAsset));
        string[] lines = file.text.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

        NUM_TIMERS = lines.Length;

        timer_table = new timerEntry[NUM_TIMERS];

        for (int i = 0; i < NUM_TIMERS; i++)
        {
            float angle = float.Parse(lines[i].Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries)[0]);
            float hitTime = float.Parse(lines[i].Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries)[1]);
            timer_table[i].isActive = true;
            timer_table[i].hitTime = hitTime + videoLatency;
            if (angle != -1.0f)
                timer_table[i].angle = angle * Mathf.PI / 180.0f;
            else
                timer_table[i].angle = Random.Range(0f, 2 * Mathf.PI);
        }

        //Start the game unpaused
        isPaused = false;
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //If we're paused, do nothing. Need to update previous frame time so songTime
        //isn't shocked on unpause
        if (isPaused)
        {
            previousFrameTime = Time.timeSinceLevelLoad;
            return;
        }

        //If the song is over, fill out the score screen and display it
        if (audio.isPlaying == false)
        {
            isPaused = true;
            comboSet.Add(currentCombo);
            Text[] textComponents = scoreMenu.GetComponentsInChildren<Text>();
            foreach (Text i in textComponents)
            {
                switch (i.text) {
                    case "SUCCESS:":
                        i.text = "SUCCESS: " + hitNotes + " / " + totalNotes;
                        break;
                    case "MISS:":
                        i.text = "MISS: " + missNotes;
                        break;
                    case "HIGHEST COMBO:":
                        comboSet.Sort();
                        comboSet.Reverse();
                        i.text = "HIGHEST COMBO: " + comboSet[0];
                        break;
                    case "TOTAL SCORE:":
                        i.text = "TOTAL SCORE: " + hitNotes;
                        break;
                    case "BEST SCORE:":
                        int bestScore = PlayerPrefs.GetInt(songName + "BestScore" + difficultyLevel, 0);
                        if (hitNotes > bestScore)
                        {
                            bestScore = hitNotes;
                            PlayerPrefs.SetInt(songName + "BestScore" + difficultyLevel, bestScore);
                            PlayerPrefs.Save();
                        }
                        i.text = "BEST SCORE: " + bestScore;
                        break;
                    case "SONGTITLE - SONGARTIST":
                        i.text = PlayerPrefs.GetString("SongTitle") + " - " + PlayerPrefs.GetString("SongArtist");
                        break;
                    case "DIFFICULTY: X":
                        i.text = "Difficulty: " + difficultyLevel;
                        break;
                    case "CHARACTER":
                        string charac = PlayerPrefs.GetString("Character");
                        if (charac == "Melody")
                            i.text = "MELODY";
                        else
                            i.text = "VICTOR";
                        break;
                }
            }
            scoreMenu.GetComponent<MenuScript>().enableMenu();
        }

        //Update combo
        comboText.text = "C O M B O : " + currentCombo;

        //Update songTime
        songTime += Time.timeSinceLevelLoad - previousFrameTime;
        previousFrameTime = Time.timeSinceLevelLoad;
        if (audio.time != lastReportedPlayheadPosition)
        {
            songTime = (songTime + audio.time) / 2;
            lastReportedPlayheadPosition = audio.time;
        }

        //Look for note timer to schedule
        for (int i = currentTimer; i < NUM_TIMERS; i++)
        {
            //If we found an unplayed note
            if (timer_table[i].isActive)
            {
                //Set angle
                note.GetComponent<NoteScript>().startAngle = timer_table[i].angle;

                //Compute time note must spawn to satisfy the hit time
                float spawnTime = timer_table[i].hitTime - spawnDistance / (velocityScale / Time.fixedDeltaTime) + .2f;

                //If the spawn time has passed, spawn the note
                if (songTime >= spawnTime)
                {
                    //Compute latency offset (perhaps do this in init?)
                    float offsetDistance = videoLatency * (velocityScale / Time.fixedDeltaTime);
                    float xOffset = offsetDistance * Mathf.Cos(note.GetComponent<NoteScript>().startAngle);
                    float yOffset = offsetDistance * Mathf.Sin(note.GetComponent<NoteScript>().startAngle);
                    note.GetComponent<Collider2D>().offset = new Vector2(noteColliderOffset.x + xOffset, noteColliderOffset.y + yOffset);

                    //Spawn the note, set timer to inactive, increment the current timer
                    Instantiate(note);
                    timer_table[i].isActive = false;
                    currentTimer++;
                    totalNotes++;
                }
                //If the spawn time hasn't passed, then all subsequent spawn times have also
                //not passed, so return
                else
                {
                    return;
                }
            }
        }
	}

    public void onMiss()
    {
        if (isPaused)
        {
            return;
        }

        if (currentCombo != 0)
        {
            comboSet.Add(currentCombo);
            currentCombo = 0;
        }
        missNotes++;
    }

    public void pause()
    {
        if (isPaused)
            return;
        isPaused = true;
        audio.Pause();
        pauseMenu.GetComponent<MenuScript>().enableMenu();
    }

    public void unpause()
    {
        StartCoroutine(unpauseHelper());
    }

    IEnumerator unpauseHelper()
    {
        pauseMenu.GetComponent<MenuScript>().disableMenu();
        for (int i = 0; i < 3; i++)
        {
            countdownText.text = (3 - i).ToString();
            yield return new WaitForSeconds(1);
        }
        countdownText.text = "";
        isPaused = false;
        audio.UnPause();
    }

    public void exit()
    {
        Application.LoadLevel("MainMenu2");
    }

    public void restart()
    {
        Application.LoadLevel("MelodyGame");
    }

    public void charselect()
    {
	    Application.LoadLevel("CharSelectScreen");
    }
}
