﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

    public Button feedback;
    public Button howToPlay;
    public Button melodyDemo;

    Animator animator;

	// Use this for initialization
	void Start () {

        animator = gameObject.GetComponent<Animator>();

        feedback = feedback.GetComponent<Button>();
        howToPlay = howToPlay.GetComponent<Button>();
        melodyDemo = melodyDemo.GetComponent<Button>();
	}

    public void disableButtons()
    {
        feedback.enabled = false;
        howToPlay.enabled = false;
        melodyDemo.enabled = false;
    }

    public void enableButtons()
    {
        feedback.enabled = true;
        howToPlay.enabled = true;
        melodyDemo.enabled = true;
    }
    public void charselectScene(){	
	GetComponent<Animation>().Play("MainMenuOut");
	Application.LoadLevel(1);
    }
    public void calibrationScene()
    {
        Application.LoadLevel(4);
    }
    public void openWebsite(string url){
	Application.OpenURL(url);
    }

    void Update()
    {
        if ((Input.GetMouseButtonDown(0) || Input.touchCount > 0) && animator.GetCurrentAnimatorStateInfo(0).IsName("RingFadeIn 0"))
        {
            animator.speed = 999;
        }
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("RingFadeIn 0"))
        {
            if (animator.speed == 1)
                return;
            animator.speed = 1;
        }
    }
}
