﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

enum Difficulty
{
    None,
    Easy,
    Medium,
    Hard
}

enum Character
{
    None,
    Melody,
    Victor
}


public class DifficultyMenuScript : MenuScript {

    public Text menuTitle;
    string songName;
    string songArtist;
    string songTitle; 

    Difficulty difficulty;
    Character character;
    
	// Use this for initialization
	new void Start () {
        base.Start();
        menuTitle = menuTitle.GetComponent<Text>();
        difficulty = Difficulty.None;
        character = Character.Melody;
        print(character + " " + difficulty);
/*
        PlayerPrefs.SetString("SongName", "catmosphere");
        PlayerPrefs.SetString("SongTitle", "Candy Coloured Sky");
        PlayerPrefs.SetString("SongArtist", "Catmosphere");
        PlayerPrefs.Save();
*/
	songName = PlayerPrefs.GetString("SongName");
	songArtist = PlayerPrefs.GetString("SongArtist");
	songTitle = PlayerPrefs.GetString("SongTitle");
	}
    public void enableMenuVictor(string title)
    {
        character = Character.Victor;
        menuTitle.text = title;
        enableMenu();
    }

    public void enableMenuMelody(string title)
    {
        character = Character.Melody;
        menuTitle.text = title;
        enableMenu();
    }

    new public void disableMenu()
    {
        character = Character.None;
        difficulty = Difficulty.None;
        base.disableMenu();
    }

    public void setMelody(){
	character = Character.Melody;
	PlayerPrefs.SetString("SongName", "catmosphere");
        PlayerPrefs.SetString("SongTitle", "Candy Coloured Sky");
        PlayerPrefs.SetString("SongArtist", "Catmosphere");
        PlayerPrefs.Save();
        print(character + " " + difficulty);
    }

    public void setVictor(){
	character = Character.Victor;
        PlayerPrefs.SetString("SongName", "sanction");
        PlayerPrefs.SetString("SongTitle", "Sanction");
        PlayerPrefs.SetString("SongArtist", "Meishi Smile");
        PlayerPrefs.Save();
        print(character + " " + difficulty);
    }	
 
    public void setEasy()
    {
        difficulty = Difficulty.Easy;
        print(character + " " + difficulty);
    }

    public void setMedium()
    {
        difficulty = Difficulty.Medium;
        print(character + " " + difficulty);
    }

    public void setHard()
    {
        difficulty = Difficulty.Hard;
        print(character + " " + difficulty);
    }

    public void playGame()
    {
        if (difficulty == Difficulty.None)
        {
            print("Please select a difficulty.");
            return;
        }
        print(character + " " + difficulty);
        PlayerPrefs.SetString("Difficulty", difficulty.ToString());
        PlayerPrefs.SetString("Character", character.ToString());
/*        if (character == Character.Melody || character == Character.Victor)
        {
            if (difficulty == Difficulty.Easy)
            {
                PlayerPrefs.SetString("SongName", "Darude_-_Sandstorm");
                PlayerPrefs.SetString("SongArtist", "Darude");
                PlayerPrefs.SetString("SongTitle", "Sandstorm");
                PlayerPrefs.Save();
            }
            else if (difficulty == Difficulty.Medium)
            {
                PlayerPrefs.SetString("SongName", "catmosphere");
                PlayerPrefs.SetString("SongArtist", "Catmosphere");
                PlayerPrefs.SetString("SongTitle", "Candy Coloured Sky");
                PlayerPrefs.Save();
            }
            else
            {
                PlayerPrefs.SetString("SongName", "sanction");
                PlayerPrefs.SetString("SongArtist", "MEISHI SMILE");
                PlayerPrefs.SetString("SongTitle", "Sanction");
                PlayerPrefs.Save();
            }

        }
*/
//	GetComponent<Animation>().Play("BGFade_v");
//      yield return new WaitForSeconds(2);
        Application.LoadLevel(2);
    }
    public void playMain(){
	Application.LoadLevel(0);
    }	

}
