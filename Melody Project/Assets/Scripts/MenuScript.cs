﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour
{

    public Canvas menu;

    Animator menuAnimator;

    // Use this for initialization
    protected void Start()
    {
        menu = menu.GetComponent<Canvas>();
        menuAnimator = menu.GetComponent<Animator>();
        menu.enabled = false;
    }

    IEnumerator delayDisable(Animator anim)
    {
        bool isClosed = false;
        while (!isClosed)
        {
            if (!anim.IsInTransition(0))
            {
                isClosed = anim.GetCurrentAnimatorStateInfo(0).IsName("Closed");
            }
            yield return new WaitForEndOfFrame();
        }

        menu.enabled = false;
    }

    public void enableMenu()
    {
        menuAnimator.SetBool("MenuOpen", true);
        menu.enabled = true;
    }

    public void disableMenu()
    {
        menuAnimator.SetBool("MenuOpen", false);
        StartCoroutine(delayDisable(menuAnimator));
    }
}
