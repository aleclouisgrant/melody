﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

    AudioSource audio;

    AudioClip[] melodyClips = new AudioClip[1];
    AudioClip[] victorClips = new AudioClip[1];

	// Use this for initialization
	void Start () {

        audio = gameObject.GetComponent<AudioSource>();

        melodyClips[0] = (AudioClip)Resources.Load("sanction_prev", typeof(AudioClip));
        victorClips[0] = (AudioClip)Resources.Load("catmosphere_prev", typeof(AudioClip));

        playMelodyPreview(0);
	
	}

    void FixedUpdate()
    {
        if (audio.isPlaying)
        {
            if (audio.volume == 0)
            {
                audio.Stop();
                audio.volume = 1.0f;
                audio.Play();
                return;
            }

            if (audio.timeSamples >= 10 * AudioSettings.outputSampleRate)
            {
                audio.volume -= 1.0f / 240.0f;
            }
        }
    }

    public void playMelodyPreview(int index)
    {
        if (audio.isPlaying)
            audio.Stop();

        audio.clip = melodyClips[index];
        audio.volume = 1.0f;
        audio.Play();
    }

    public void playVictorPreview(int index)
    {
        if (audio.isPlaying)
            audio.Stop();

        audio.clip = victorClips[index];
        audio.volume = 1.0f;
        audio.Play();
    }
}
