﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

enum Difficulty{
	None,
	Easy,
	Medium,
	Hard
}

enum Character{
	None,
	Melody,
	Victor
}

public class CharSelectScript : MenuScript {

	public Text menuTitle;

	Difficulty difficulty;
	Character character;

	new void Start(){
		base.Start();
		menuTitle = menuTitle.GetComponent<Text>();
		difficulty = Difficulty.Easy;
		character = Character.Melody;
	}

/*	public void enableMenuMelody(string title){
		character = Character.Melody;
		menuTitle.text = title;
		enableMenu();
	}
	public void enableMenuVictor(){
		character = Character.Victor;
		menuTitle.text = title;
		enableMenu();
	}
*/
	new public void disableMenu(){
		character = Character.None;
		difficulty = Difficulty.None;
		base.disableMenu();
	}

	public void setMelody(){
		character = Character.Melody;
		animation.Play("BGFade_m");
		SetString("M E L O D Y");
	}
	public void setVictor(){
		character = Character.Victor;
		animation.Play("BGFade_v");
		text("V I C T O R");
	}

	public void setNone(){
		difficulty = Difficulty.None;
	}
	public void setEasy(){
		difficulty = Difficulty.Easy;
		animation.Play("DiffFade1");
	}
	public void setMedium(){
		difficulty = Difficulty.Medium;
		animation.Play("DiffFade2");
	}
	public void setHard(){
		difficulty = Difficulty.Hard;
		animation.Play("DiffFade3");
	}

	public void playGame(){
		if (difficulty == Difficulty.None){
			print("Please select a difficulty.");
			return;
		}
		print(character + " " + difficulty);
		PlayerPrefs.SetString("Difficulty", difficulty.ToString());
		PlayerPrefs.SetString("Character", character.ToString());
		if (character == Character.Melody){
			PlayerPrefs.SetString("SongName", "sanction");
			PlayerPrefs.SetString("SongTitle", "Sanction");
			PlayerPrefs.SetString("SongArtist", "Meishi Smile");
			PlayerPrefs.Save();
		}
		else if (character == Character.Victor){
			PlayerPrefs.SetString("SongName", "catmosphere");
			PlayerPrefs.SetString("SongTitle", "Candy Coloured Sky");
			PlayerPrefs.SetString("SongArtist", "Catmosphere");
			PlayerPrefs.Save();
		}
//		animation.Play();
//		yield return new WaitForSeconds(2);
		Application.LoadLevel(2);
	}

	public void playMain(){
		Application.LoadLevel(0);
	}

}
