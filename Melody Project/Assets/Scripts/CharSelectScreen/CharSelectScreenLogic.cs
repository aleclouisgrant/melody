﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

enum Difficulty
{
    None,
    Easy,
    Medium,
    Hard
}

enum Character
{
    None,
    Melody,
    Victor
}

public class CharSelectScreenLogic : MonoBehaviour {

    Character character;
    Difficulty difficulty;

    Text charName;
    Text song;
    Text highScore;

	// Use this for initialization
	void Start () {

        Text[] textComponents = gameObject.GetComponentsInChildren<Text>();
        character = Character.Melody;
        difficulty = Difficulty.None;

        foreach (Text component in textComponents)
        {
            switch (component.text)
            {
                case "C H A R   N A M E":
                    charName = component;
                    break;
                case "SONG NAME - SONG ARTIST":
                    song = component;
                    break;
                case "SCORE":
                    highScore = component;
                    break;
            }
        }

        toMelody();
	}

    public void toVictor()
    {
        character = Character.Victor;
        charName.text = "V I C T O R";
        song.text = "CANDY COLORED SKY - CATMOSPHERE";
        highScore.text = "";
        PlayerPrefs.SetString("Character", "Victor");
        PlayerPrefs.SetString("SongName", "catmosphere");
        PlayerPrefs.SetString("SongArtist", "Catmosphere");
        PlayerPrefs.SetString("SongTitle", "Candy Colored Sky");	
        return;
    }

    public void toMelody()
    {
        character = Character.Melody;
        charName.text = "M E L O D Y";
        song.text = "SANCTION - MEISHI SMILE";
        highScore.text = "";
        PlayerPrefs.SetString("Character", "Melody");
        PlayerPrefs.SetString("SongName", "sanction");
        PlayerPrefs.SetString("SongArtist", "MEISHI SMILE");
        PlayerPrefs.SetString("SongTitle", "Sanction");
	return;
    }

    public void setEasy()
    {
        difficulty = Difficulty.Easy;
		PlayerPrefs.SetString("DifficultyLevel","1");
        if (character == Character.Melody)
        {
            highScore.text = PlayerPrefs.GetInt("sanctionBestScore1").ToString();
        }
        else
        {
            highScore.text = PlayerPrefs.GetInt("catmosphereBestScore1").ToString();
        }
	    return;
    }

    public void setMedium()
    {
        difficulty = Difficulty.Medium;
		PlayerPrefs.SetString("DifficultyLevel","2");
        if (character == Character.Melody)
        {
            highScore.text = PlayerPrefs.GetInt("sanctionBestScore2").ToString();
        }
        else
        {
            highScore.text = PlayerPrefs.GetInt("catmosphereBestScore2").ToString();
        }
	    return;
    }

    public void setHard()
    {
        difficulty = Difficulty.Hard;
		PlayerPrefs.SetString("DifficultyLevel","3");
        if (character == Character.Melody)
        {
            highScore.text = PlayerPrefs.GetInt("sanctionBestScore3").ToString();
        }
        else
        {
            highScore.text = PlayerPrefs.GetInt("catmosphereBestScore3").ToString();
        }
	    return;
    }

    public void play()
    {
        if (difficulty == Difficulty.None)
            return;
        Application.LoadLevel("MelodyGame");
    }

    public void mainMenu()
    {
        Application.LoadLevel("MainMenu2");
    }
}
